---
layout:     post
title:     FileFilter 的使用
subtitle:       
date:     2020-09-18
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Java基础
    -   
    -   
---  

	### 1. 作用

+ 用于筛选指定格式的文件

### 2. 具体用法

``` java
public class FileUtil {

    public void myListFiles(String dir) {

        File directory = new File(dir);

        if (!directory.isDirectory()) {
            System.out.println("No directory provided");
            return;
        }

        File[] files = directory.listFiles(filefilter);

        for (File f : files) {
            System.out.println(f.getName());
        }
    }

    //这个文件过滤器会过滤掉文件后缀不是.txt的文件，当返回为false时则丢弃文件
    FileFilter filefilter = new FileFilter() {

        public boolean accept(File file) {
            //if the file extension is .txt return true, else false
            if (file.getName().endsWith(".txt")) {
                return true;
            }
            return false;
        }
    };

    public static void main(String[] args) {
        FileUtil fileutil = new FileUtil();
        fileutil.myListFiles("D:\\\\temp");
    }
}
```

