---
layout:     post
title:      Python  入门小项目之学生信息管理系统（txt版）
subtitle:         
date:       2020-08-03
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - Python
    -   
    -   
---

###  下面这个小项目是一个简易版的学生信息管理系统，业务逻辑比较简单，主要用来联系python的基本语法



### 项目结构：

![image-20200803145901202](/img/noteImage/image-20200803145901202.png)

### 代码

``` python
# -*- coding: utf-8 -*-
# @Time    : 2020-08-02 19:32:51
# @Author  : MrGeek
# @FileName: SIMS.py
# @Software: PyCharm
# @Blog    ：https://mrgeek-zrh.gitee.io

"""
学生信息管理系统
1.学生信息存储：
    使用txt文件进行学生信息的存储
2.设计思路：
2.1.读取信息：
    将数据从文件中全部读出，临时存到列表中，列表中的元素的类型：实体类
2.2.写入信息：
    将列表中的数据以w的形式写入到文件中
"""


# 创建实体类
class Student:
    """
    学生实体类，每一个属性都和student.txt 中的每一个字段相对应
    """
    name = ""
    num = ""
    sex = ""
    age = 0
    def __str__(self):
        return "Student\n name: %s \t num: %s \t sex: %s \t age: %d" %(self.name, self.num,self.sex,self.age)

# 从文件中读取数据到列表中
def ReadStudentInfoFromStudentTxt():
    """
    将数据从文件中读出，存入列表中
    Returns:
        Students(list):存有学生信息的列表，列表中的每个元素都是一个Student实体类
    """

    # 创建用于临时存储所有学生信息的列表
    Students = []

    # 从文件中读取信息（成行读取）
    f = open("student.txt")
    line = f.readline()
    # 获取每行字符串的长度
    length = len(line)
    # 去除每行字符串末尾的 ‘\n’
    newLine = line[0:length-1]
    # 分割字符串，方便封装数据
    student = newLine.split(" ")
    while newLine:
        # 封装数据，将数据封装到实体类中
        studentList = Student()
        studentList.name = student[0]
        studentList.num = student[1]
        studentList.sex = student[2]
        studentList.age = int(student[3])

        # 将实体类添加到列表中
        Students.append(studentList)

        # 再次重复读取
        line = f.readline()
        # 获取每行字符串的长度
        length = len(line)
        # 去除每行字符串末尾的 ‘\n’
        newLine = line[0:length - 1]
        # 分割字符串，方便封装数据
        student = newLine.split(" ")

    # 返回
    return Students

# 将列表中的数据写入到文件中
def WriteStudentInfoIntoStudentTxt(students):
    """
    将列表中的信息写入到txt文件中
    Args:
        students: 存有所有学生信息的列表

    """
    f = open("student.txt",mode="w",encoding="utf-8")
    for i in students:
        content = i.name+" "+i.num+" "+i.sex+" "+str(i.age)+"\n"
        f.write(content)


# 编写CURD函数
def addStudentInfoIntoStudentTxt(student):
    """
    添加学生信息
    Args:
        student:存有学生信息的Student类型实体类

    Returns:包含有所有学生信息的列表

    """
    Students = ReadStudentInfoFromStudentTxt()
    Students.append(student)
    return Students

def deleteStudentInfoByStudentName(name):
    """
    根据学生姓名删除学生信息
    Args:
        name:要删除的学生的姓名

    Returns:包含有所有学生信息的列表

    """
    students = ReadStudentInfoFromStudentTxt()
    for i in students:
        if i.name == name:
            students.remove(i)
            break
    WriteStudentInfoIntoStudentTxt(students)
    return students

def updateStudentInfoByStudentName(student):
    """
    根据学生姓名修改学生信息
    Args:
        student:包含有学生信息的Student实体类

    Returns:包含有所有学生信息的列表

    """
    students = ReadStudentInfoFromStudentTxt()
    for i in students:
        if i.name == student.name:
            i.num = student.num
            i.sex = student.sex
            i.age = student.age
            break
    WriteStudentInfoIntoStudentTxt(students)
    return students

def findStudentInfoByStudentName(name):
    """
    根据学生姓名查询学生信息
    Args:
        name:学生姓名

    Returns:学生信息实体类

    """
    students = ReadStudentInfoFromStudentTxt()
    for i in students:
        if i.name == name:
            return i

def findAllStudentInfo():
    students = ReadStudentInfoFromStudentTxt()
    for i in students:
        print(i)


# 编写界面函数
def makeUI():
    print("|------------------------------|")
    print("|	1.添加学生信息				  |")
    print("|	2.删除学生信息 			  |")
    print("|	3.修改学生信息      		  |")
    print("|	4.查询学生信息      		  |")
    print("|	5.展示学生信息      		  |")
    print("|							  |")
    print("|------------------------------|")
    print("请输入你的选择：")


# 编写系统的组织函数
def systemMainFunction():
    while True:
        makeUI()
        choice = input()
        student = Student()
        if choice =='1':
            print("请输入你想要添加的学生的姓名：")
            student.name = input()
            print("请输入你想要添加的学生的学号：")
            student.num = input()
            print("请输入你想要添加的学生的性别：")
            student.sex = input()
            print("请输入你想要添加的学生的年龄：")
            student.age = int(input())
            addStudentInfoIntoStudentTxt(student)
        elif choice == '2':
            print("请输入你想删除的学生的姓名：")
            name = input()
            deleteStudentInfoByStudentName(name)
        elif choice == '3':
            student1 = Student()
            print("请输入你想修改的学生的姓名：")
            student1.name = input()
            print("请输入你想要修改的学生的学号：")
            student1.num = input()
            print("请输入你想要修改的学生的性别：")
            student1.sex = input()
            print("请输入你想要修改的学生的年龄：")
            student1.age = int(input())

            updateStudentInfoByStudentName(student1)
        elif choice == '4':
            print("请输入你想查询的学生的姓名：")
            name = input()
            student2 = findStudentInfoByStudentName(name)
            print(student2)
        elif choice == '5':
            findAllStudentInfo()
        else:
            print("没有该选项！！！")
        print("你是否要继续操作？？？ (n:否，y：是)")
        choice1 = input()
        if choice1 == 'y':
            continue
        elif choice1 == 'n':
            break
        else:
            record = 0
            print("没有该选项！！!请重新选择：（y/n）")
            while True:
                choice2 = input()
                if choice2 == 'y':
                    break
                elif choice2 == 'n':
                    record = 1
                    break
                else:
                    print("没有该选项！！！请重新选择：（y/n）")
            if record == 1:
                break

# 启动系统
systemMainFunction()

# 测试界面函数
# makeUI()

# 测试查找功能
# student = findStudentInfoByStudentName("张3")
# print(student)

# 测试修改功能
# student = Student()
# student.name = "张4"
# student.num = "2019082017"
# student.age = 13
# student.sex = "男"
# students = updateStudentInfoByStudentName(student)
#
# for i in students:
#     print(i)


# 测试删除功能
# students = deleteStudentInfoByStudentName("张3")
# for i in students:
#     print(i)

# 测试添加功能
# student = Student()
# student.age = 12
# student.name = "张6"
# student.sex = "男"
# student.num = "2019082022"
# Students = addStudentInfoIntoStudentTxt(student)
# WriteStudentInfoIntoStudentTxt(Students)
# for i in Students:
#     print(i)


# 测试调用函数
# Students = ReadStudentInfoFromStudentTxt()

```





