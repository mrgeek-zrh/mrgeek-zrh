---
layout:     post
title:     对ThreadLocal 的说明
subtitle:       
date:     2020-09-16
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Java基础
    -   
    -   
---  

# 1. 什么是ThreadLocal

ThreadLocal 本质上就是一个容器，将变量放进ThreadLocal 中后，该变量就只能被当前线程所调用，而不能被其他线程所调用，可以保证改变量的绝对线程安全

# 2. 作用：

+ 将变量放进ThreadLocal 中后，该变量就只能被当前线程所调用，而不能被其他线程所调用，可以保证改变量的绝对线程安全

##  代码演示：

### 没有使用ThreadLocal的情况：

**1. 先定义一个接口**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005154536272.png)

**2. 创建一个线程类**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005154651389.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005154717847.png)

**3. 编写接口的实现类，并开启多线程**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005154834859.png)

**4. 运行结果**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005154908896.png)



### 使用了ThreadLocal的情况

只需对接阔实现类有所改变即可

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155147606.png)

**2. 运行结果**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155223404.png)

# 3. 自己实现ThreadLoacl

``` java
public class MyThreadLocal<T>{
    private Map<Thread,T>container = Collections.synchronizedMap(new HashMap<Thread,T>());
    public void set (T value){
        container.put(Thread.currentThread(),value);
    }
    public T get(){
        Thread thread = Thread.currentThread();
        T value = container.get(thread);
        if(value==null&&!container.containsKey(thread)){
            value = initialValue();
            container.put(thread,value);
        }
        return value;
    }
    public void remove(){
        container.remove(Thread.currentThread());
    }
    protected T initialValue(){
        return null;
    }
}
```

# 4. ThreadLocal使用案例

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155420936.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155455821.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155534653.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155739400.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155903458.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005155950972.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005160103727.png)

<img src="https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005160250436.png" />

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201005160404146.png)



