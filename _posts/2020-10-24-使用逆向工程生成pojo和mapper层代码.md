---
layout:     post
title:     使用逆向工程生成pojo和mapper层代码
subtitle:       
date:     2020-10-24
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - 逆向工程
    -   
    -   
---  

### 1. 导入逆向工程需要的工程代码

链接: https://pan.baidu.com/s/1LBHeUT66W-a-bohs4ZvyDg  密码: 3t45

### 2. 修改逆向工程中的`generatorConfig.xml` 文件

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201025004525155.png)



![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201025004822874.png)



![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201025004923459.png)



### 3. 运行`GeneratorSqlmap.java` 文件

运行之后，生成的`.java` 文件和 `.xml` 文件就可以在上面指定的文件夹下找到了。

### 大功告成！！！