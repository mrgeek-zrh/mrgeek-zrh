---
layout:     post
title:     getclassloader.getresource()
subtitle:       
date:     2020-09-17
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Java基础
    -   
    -   
---  

1. getClass().getClassLoader().getResource(fileName)：表示只会在*根目录*下（/）查找该文件；
   + getClassLoader() 确定了是从 / 下开始查找的
   + 返回值：URL类

### 补充

getClassLoader().getResource() 和 getResource() 的区别

+ https://www.cnblogs.com/tinaluo/p/12357381.html