---
layout:     post
title:     ServletContext类的总结
subtitle:       
date:     2020-09-21
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - Servlet
    -   
    -   
---  

### 1. 什么是ServletContext？

在将web项目部署到服务器（比如tomcat）后，`服务器会为每一个工程创建一个对象`，这个对象就是ServletContext。

此外，ServletContext还是一个域对象。

ServletContext对象被包含在ServeltConfig对象中，可以通过如下方式获取ServletContext

```java
//以下两种方法效果相同
this.getServletContext();
servletConfig.getServletContext();
```

servletConfig 实例一般不用我们自己创建，一般web服务器会自动将`<servlet>`标签下配置信息封装成servletConfig

**什么是域对象？**

域对象是服务器在内存上创建的存储空间，用于在不同动态资源（servlet）之间传递与共享数据。

**域对象的特点**

凡是域对象都有如下3个方法：

| setAttribute(name,value);name是String类型，value是Object类型； | 往域对象里面添加数据，添加时以key-value形式添加 |
| ------------------------------------------------------------ | ----------------------------------------------- |
| getAttribute(name);                                          | 根据指定的key读取域对象里面的数据               |
| removeAttribute(name);                                       | 根据指定的key从域对象里面删除数据               |

**代码演示 :**

域对象存储数据AddDataServlet代码：

```java
publicvoid doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException ,IOException {
         //往serlvetContext里面存数据
         getServletContext().setAttribute("username", "admin");
         response.getOutputStream().write("用户名写入到servletContext成功".getBytes());
     }

```

获取域对象数据GetDataServlet代码

```java
public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取ServletContext里面的用户名数据
		Object valueObject = getServletContext().getAttribute("username");
		if(valueObject!=null){
            //打印数据到页面
			response.getOutputStream().write(("从servletContext读取到的用户名数		据："+valueObject.toString()).getBytes());
         }
}
```

**将数据打印到网页上的另一种方法**

``` java
//显示到网页上
PrintWriter out=response.getWriter();
out.println("没有移除属性之前name属性的值是："+name);
```

### 2. ServletContext的特点

1. 这个对象全局唯一
2. 工程内部所有的Servlet都共享该对象，共享的内容包括该对象中存储的所有动态资源
   - 基于以上两点，ServletContext又被称为全局应用程序共享对象

3. 该对象在服务器启动时创建，服务器关闭时销毁

### 3. 主要用法

#### 3.1 读取全局配置参数

**核心代码：**

```java
getServletContext().getInitParameter(name);//根据指定的参数名获取参数值
getServletContext().getInitParameterNames();//获取所有参数名称列表
```

**具体代码演示：**

1. 在web.xml中配置全局参数

```xml
  <!-- 全局配置参数，因为不属于任何一个servlet，但是所有的servlet都可以通过servletContext读取这个数据 -->
  <context-param>
      <param-name>param1</param-name>
      <param-value>value1</param-value>
  </context-param>
   <context-param>
       <param-name>param2</param-name>
       <param-value>value2</param-value>
  </context-param>
```

2. 在动态资源servlet里面使用servletcontext读取全局参数代码

``` java
public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
	//使用servletContext读取全局配置参数数据
    //核心方法
    /*getServletContext().getInitParameter(name);//根据指定的参数名获取参数值
           getServletContext().getInitParameterNames();//获取所有参数名称列表*/
	//打印所有参数
    
    //1.先获取所有全局配置参数名称
    Enumeration<String> enumeration =  getServletContext().getInitParameterNames();
    //2.遍历迭代器
    while(enumeration.hasMoreElements()){
        //获取每个元素的参数名字
        String parameName = enumeration.nextElement();
        //根据参数名字获取参数值
        String parameValue = getServletContext().getInitParameter(parameName);
        //打印
        System.out.println(parameName+"="+parameValue);
    }
}
```

#### 3.2 搜索当前工程目录下的资源文件

只能读取webContent目录下的资源，无法读取src下的资源，src下的资源需要通过类加载器获取

**核心方法：**

```java
//根据相对路径获取服务器上资源的绝对路径
getServletContext().getRealPath(path)
//根据相对路径获取服务器上资源的输入字节流
getServletContext().getResourceAsStream(path)
```

#### 3.3 获取当前工程的名字

**核心代码：**

```java
getServletContext().getContextPath()；
```

#### 3.4 用于多个Servlet之间共享数据

#### 3.5 实现Servlet转发

通过ServletContext可以实现页面的转发，和我们的request.getRequestDispatcher("资源名").forward(request,response);的用法一样。可以共享request对象，也是在服务器中发生页面跳转，只能访问该web应用下的资源。

``` java
package com.yd.serlvlet;
 
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
/**
 * Servlet implementation class Forward
 */
@WebServlet("/Forward")
public class Forward extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Forward() {
        super();
        // TODO Auto-generated constructor stub
    }
 
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//直接转发
		request.setCharacterEncoding("utf-8");
		//给request对象添加属性
		request.setAttribute("age", 18);
		//通过servletcontext实现转发
		this.getServletConfig().getServletContext().getRequestDispatcher("/GetInitParam").forward(request, response);
	}
 
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
 
}
```

#### 3.5 管理Servlet

通过ServletContext对象，我们可以向Servlet容器中注入Servlet、实例化一个Servlet（创建一个Servelt）、从Servlet容器（比如tomcat）中获取Servlet的注册信息等等

**1. 向servlet容器中注入Servlet**

+ addServlet()

addServlet()提供了编程式的向servlet容器中注入servlet的方式，其达到的效果和在web.xml中配置或者使用WebServlet注解配置servlet是一样的。

**返回值：** ServletRegistration实例

**2. 获取指定Servlet的注册信息**

+ getServletRegistration()

**返回值：** ServletRegistration 实例

**3. 获取当前ServletContext中所有的Servlet的注册信息**

+ getServletRegistrations

**返回值：** Map<String, ? extends ServletRegistration>

