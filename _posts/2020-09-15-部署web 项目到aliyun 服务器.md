---
layout:     post
title:     部署web 项目到aliyun 服务器
subtitle:       
date:     2020-09-15
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - linux
    -   
    -   
---  

### 1. 给服务器配置java 环境

#### 1.1 上传jdk1.8 到服务器到指定文件夹

> scp /home/mrgeek/download/software2/jdk-8u251-linux-x64.tar.gz 服务器用户名@服务器公网ip:~/

#### 1.2 进入压缩包所在文件夹，解压缩

> sudo tar -zxvf jdk-8u251-linux-x64.tar.gz

#### 1.3 配置环境变量

+ 将以下内容写到 /etc/bash.bashrc 中

> export JAVA_HOME=/usr/local/java/jdk1.8.0_25  
>
> export JRE_HOME=${JAVA_HOME}/jre  
>
> export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
>
>  export PATH=${JAVA_HOME}/bin:$PATH

#### 1.4 使环境变量生效

> source /etc/bash.bashrc

#### 1.5 测试是否配置成功

> java -version

### 2. 配置tomcat

#### 2.1 上传tomcat安装包到服务器

> scp /home/mrgeek/download/software2/apache-tomcat-8.5.57.tar.gz 服务器用户名@服务器公网ip:~/

#### 2.2 解压缩

> tar -zxvf apache-tomcat-8.5.57.tar.gz

#### 2.3 配置环境变量

+ 将以下内容写到 /etc/bash.bashrc 中

> export TOMCAT_HOME=/opt/apache-tomcat-8.5.57

#### 2.4 使配置文件生效

> source /etc/bash.bashrc

#### 2.5 测试是否配置成功

- 先进入解压得到的文件夹，里面有一个bin 文件夹，进入bin 文件夹，里面有一个启动脚本：startup.sh ,运行这个脚本文件即可启动tomcat
- 比如我现在进入了bin 文件夹，然后执行下面的命令开启tomcat：

> sudo ./startup.sh
>
> 开启之后，可以在浏览器中输入： 服务器的公网ip:8080 测试是否安装成功

##### 别忘了开放服务器的8080端口！！！

+ 关闭tomcat 

+ 仍然是这个文件夹下：

+ > sudo ./shutdown.sh

### 3. 安装mysql

> apt install 自己的mysql版本

+ 安装完之后可以通过`service mysql start` 启动mysql服务

### 4. 导入sql文件

+ 具体做法参见前面的博文

### 5. 将写好的web项目打包成war包并部署到服务器

#### 5.1 打包成war包

+ 借助eclipse

- 在eclipse 上右键项目，选择export -> war file ,然后导出到指定的位置即可

#### 5.2 上传war包到tomcat 的webapps 目录下

+ 上传方式和上面相同

#### 5.3 启动tomcat

#### 5.4 浏览器输入：`http://服务器公网ip:8080/项目名` 访问项目

### 大功告成！！！





