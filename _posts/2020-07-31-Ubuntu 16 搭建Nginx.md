---
layout:     post
title:      Ubuntu 18 搭建Nginx
subtitle:       Nginx
date:       2020-07-31
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - Nginx
    -   
    -   
---

# 1. 下载Nginx1.1.8 压缩包

+ 官网下载：http://nginx.org/en/download.html

+ 百度网盘下载：
  + 链接: https://pan.baidu.com/s/1G4bTp2ZkNJR_9KwSfaAwtw  
  + 密码: s4e6

# 2. 安装Nginx所依赖的包

+ nginx是C语言开发，所以需要安装C 语言的运行环境

+ 需要的包：

  + gcc

    + > sudo apt-get install gcc

  + PCRE

    + > ```
      > sudo apt-get install libpcre3 libpcre3-dev
      > ```

  + zlib

    + > ```
      > sudo apt-get install zlib1g-dev
      > ```

  + openssl

    + > ```
      > sudo apt-get install openssl libssl-dev
      > ```

# 3. 解压压缩包

> sudo  tar -zxvf 包名

# 4. 进入压缩包，进入到configue 可执行文件所在目录，执行以下语句：

> ./configure \--prefix=/usr/local/nginx \--pid-path=/var/run/nginx/nginx.pid \--lock-path=/var/lock/nginx.lock \--error-log-path=/var/log/nginx/error.log \--http-log-path=/var/log/nginx/access.log \--with-http_gzip_static_module \--http-client-body-temp-path=/var/temp/nginx/client \--http-proxy-temp-path=/var/temp/nginx/proxy \--http-fastcgi-temp-path=/var/temp/nginx/fastcgi \--http-uwsgi-temp-path=/var/temp/nginx/uwsgi \--http-scgi-temp-path=/var/temp/nginx/scgi

# 5. make

# 6. make install

# 7. 开启nginx 服务

+ 经过上面的操作，会在/usr/local/下生成一个nginx 文件夹，进入里面的sbin文件夹，会有一个名为nginx 的可执行文件 ，通过命令 `./nginx `即可开启nginx 服务

+ 开启Nginx 之后，就可以通过电脑的ip进行访问（默认开放80端口提供服务）
  + 对于服务器，如果无法访问，查看一下是否开放了80 端口

# 8. 关闭nginx 服务

> ./nginx -s stop

# 9. 刷新配置

+ 在修改nginx 的一些配置文件之后，要刷新一下nginx

+ > ./nginx -s reload

# 到此，Nginx 安装完毕！！！



