---
layout:     post
title:     linux minit 使用picgo 搭建gitee图床
subtitle:       
date:     2020-09-12
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     

    - linux
    -   
    
    -   
---

### 1. 下载nodejs npm 和 picgo

> nodejs: apt install nodejs
>
> npm : apt install npm
>
> picgo : https://github.com/Molunerfinn/PicGo/releases
>
> ![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912095817167.png) 

### 2. 打开picgo

+ picgo 为AppImage 文件
+ 打开时需要 允许该文件以程序执行文件 运行
  + ![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912100132130.png)

### 3. 安装gitee插件

+ 点击插件设置，搜索gitee
  + ![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912100316072.png)
    + 安装右边那个

### 4. 在gitee 中创建一个用于存储图片的仓库（必须设为public）

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912100543996.png)

### 5. 配置gitee插件并设置gitee 为默认图床

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912101035482.png)

+ 选择 ` 配置uploader-gitee` 

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912101455137.png)

然后设置gitee 为默认图床

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912101631320.png)

然后在PicGo 设置中设置自己喜欢的一些快捷方式

### 6. 设置typora 

+ 打开偏好设置，找到图像

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912101930063.png)

### 然后就可以了

