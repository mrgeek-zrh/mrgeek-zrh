---
layout:     post
title:      linux  配置java 环境
subtitle:       jdk、tomcat、maven、mysql、eclipse
date:       2020-07-31
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - linux
    -   
    -   
---

# 0. 以下安装适用于debian 系的发行版

# 1. jdk1.8 安装

+ 其他版本的同理

## 1.1 下载 jdk1.8

+ 官网下载：https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html
+ 百度网盘下载：
  + 链接: https://pan.baidu.com/s/1I_nXtTofDb79JwyITiIatw  密码: grr0

## 1.2 解压下载的安装包

> sudo tar -zxvf 压缩包名

## 1.3 配置环境变量

+ linux 中配置环境变量的方式有很多，比如在 home 下的.bashrc  中添加，或在 /etc/profile中添加 还可以在 /etc/bash/bash.rc 中添加
  + 这三个文件的系统优先级不同，对于个人用户，建议在 /etc/bash.bashrc 中配置，以免由于linux 的分组问题导致有时候配置的环境变量失效
  + 一下的所有配置均在/etc/bash.bashrc中进行

### 1.3.1 要配置的内容

+ 将一下内容写到 /etc/bash.bashrc 中

> export JAVA_HOME=/usr/local/java/jdk1.8.0_25 
> export JRE_HOME=${JAVA_HOME}/jre 
> export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib export PATH=${JAVA_HOME}/bin:$PATH

+ 其中JAVA_HOME 是你自己解压出来的jdk 路径

+ 如果碰到无法该文件只允许 读 的情况，先输入一下命令，再写入上面的配置内容：

+ > chmod -R 777 /etc/bash.bashrc

### 1.3.2 使配置的内容生效

> source /etc/bash.bashrc

### 1.3.3 测试jdk 是否安装成功

+ 输入一下命令：

+ > java -version

+ 输出以下内容，说明·jdk 配置成功：
+ ![image-20200731095728460](/img/noteImage/image-20200731095728460.png)

# 2. 安装tomcat8

+ 安装tomcat 之前必须先安装jdk 

+ tomcat 的其他版本同理

## 2.1 下载tomcat8

+ 官网下载：http://tomcat.apache.org/download-80.cgi
+ ![image-20200731100337582](/img/noteImage/image-20200731100337582.png)

+ 百度网盘下载：

## 2.2 解压下载的压缩包

> sudo tar -zxvf 包名

## 2.3 配置环境变量

### 2.3.1 要配置的内容：

+ 将以下内容添加到 /etc/bash.bashrc

+ > JAVA_HOME=/usr/lib/jvm/java-8-oracle
  > JRE_HOME=/usr/lib/jvm/java-8-oracle/jre
  > CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar:$JRE_HOME/lib
  > PATH=$JAVA_HOME/bin:$JRE_HOME/bin:$PATH
  > TOMCAT_HOME=/opt/apache-tomcat-8.0.47

  + 前四行的jdk 环境变量的配置如果前面配好了，这里就不用再配置了，只需配置最后一行即可（后面的路径换成自己的）

### 2.3.2 使配置内容生效：

> sourec /etc/bash.bashrc

### 2.3.3 测试是否安装成功

+ 先进入解压得到的文件夹，里面有一个bin 文件夹，进入bin 文件夹，里面有一个启动脚本：startup.sh ,运行这个脚本文件即可启动tomcat 
+ 比如我现在进入了bin 文件夹，然后执行下面的命令开启tomcat：

> sudo ./startup.sh 
>
> 开启之后，可以在浏览器中输入： localhost:8080 测试是否安装成功

+ 关闭tomcat:

> sudo ./shutdown.sh

# 3. 安装maven3.6.3

## 3.1 下载maven

+ 官网下载：https://maven.apache.org/download.cgi#

![image-20200731102156229](/img/noteImage/image-20200731102156229.png)

+ 百度网盘下载：
  + 链接: https://pan.baidu.com/s/1mtnq2YjzII1PujB_LiffeQ  
  + 密码: 2968

## 3.2 解压下载的安装包

> sudo tar -zxvf 压缩包名

## 3.3 配置环境变量

### 3.3.1 要配置的内容

#### 3.3.1.1 环境变量

+ 在/etc/bash.bashrc 中添加下面两句命令

> export M2_HOME=/usr/local/apache-maven-3.5.3 export PATH=${M2_HOME}/bin:$PATH

+ M2_HOME 后面是自己的路径

#### 3.3.1.2 setting.xml 配置

1. ![image-20200731114318290](/img/noteImage/image-20200731114318290.png)

2. 修改setting.xml 中的内容
   1. ![image-20200731114915679](/img/noteImage/image-20200731114915679.png)
   2. ![image-20200731115105770](/img/noteImage/image-20200731115105770.png)

+ 镜像内容：

  ``` xml
  <mirror>
     <id>alimaven</id>
     <name>aliyun maven</name>
     <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
     <mirrorOf>central</mirrorOf>        
   </mirror>
  ```

### 3.3.2 使配置的内容生效

> source /etc/bash.bashrc

### 3.3.3 测试

> mvn -v

+ 输出类似于下面的语句说明配置成功：
+ ![image-20200731103149006](/img/noteImage/image-20200731103149006.png)

# 4. 安装eclipse

+ 安装eclipse 之前要先安装好JDK

## 4.1 下载eclipse

+ 官网下载：http://ftp.jaist.ac.jp/pub/eclipse/technology/epp/downloads/release/photon/R/eclipse-jee-photon-R-linux-gtk-x86_64.tar.gz
+ 网盘下载：
  + 链接: https://pan.baidu.com/s/1jjXwfeXPIqWQ-VK7UML4CQ  
  + 密码: 76q8

## 4.2 解压压缩包

>  sudo tar -zxvf 压缩包名

## 4.3 启动eclipse

+ 进入解压得到的文件夹(一般叫eclipse），里面有一个叫eclipse的可执行文件，运行一下命令启动eclipse:

+ > ./ eclipse

+ 如果启动时，发生错误，解决方案：

  1. 将前面解压的 jdk 压缩包里面的 jre 文件夹 拷贝到 解压eclipse 压缩包得到的文件夹中
     - 借助cp 命令
  2. 确保你电脑安装的jdk 使1.8 及以上，由于eclipse 运行也依赖于jdk ，且最新版的eclipse 不支持jdk 1.8 一下的版本，所以我们要安装jdk 1.8 以上版本

### 4.3.1 为eclipse创建桌面快捷方式

+  在桌面建一个名为 eclipse.desktop 的文件

+ 在文件中写入一下内容：

+ > [Desktop Entry]
  > Encoding=UTF-8
  > Name=Eclipse
  > Comment=Eclipse IDE
  > Exec=/home/mrgeek/download/software2/eclipse
  > Icon=/home/mrgeek/download/software2/icon.xpm
  > Terminal=false
  > Type=Application
  > Categories=GNOME;Application;Development;
  > StartupNotify=true

  + Exec 和 Icon 填写自己的路径

### 4.3.2 将eclipse 添加到启动器中

> sudo cp ~/Desktop/eclipse.desktop /usr/share/applications

+ 有可能你的电脑的桌面文件夹叫：桌面，而不是 Desktop,改一下即可

## 4.4 为eclipse 配置jdk 1.8

1. ![](/img/noteImage/2020-07-31_11-28.png)

2. ![image-20200731113024621](/img/noteImage/image-20200731113024621.png)

3. ![image-20200731113058193](/img/noteImage/image-20200731113058193.png)

4. 选择你自己的jdk 所在文件夹即可

## 4.5 为eclipse 配置tomcat

1. ![image-20200731113340333](/img/noteImage/image-20200731113340333.png)

2. ![image-20200731113536028](/img/noteImage/image-20200731113536028.png)

3. ![image-20200731113600430](/img/noteImage/image-20200731113600430.png)

4. 选择自己的tomcat的文件即可

## 4.6 为eclipse 配置maven

1. ![image-20200731113808293](/img/noteImage/image-20200731113808293.png)

2. ![image-20200731113945152](/img/noteImage/image-20200731113945152.png)

# 到此，所有的环境就都配好了

