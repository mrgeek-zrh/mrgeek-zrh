---
layout:     post
title:     JDBC + Druid总结
subtitle:       
date:     2020-09-12
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - mysql
    -   
    -   
---  

### 1. 导入jar包

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20200912161146644.png)

### 2. 和数据库建立连接

``` java
public static Connection getConnection() throws ClassNotFoundException, SQLException {
		
		//1.加载驱动
		//确定链接的数据库的类型
		Class.forName("com.mysql.jdbc.Driver");
		//确定请求路径
		String url = "jdbc:mysql://112.124.28.54:3306/studentInfo?characterEncoding=UTF-8";
		//确定用户名
		String user = "root";
		//确定密码
		String password = "20020920z";
		
		//2.建立连接
		Connection connection = (Connection) DriverManager.getConnection(url,user,password);
		return connection;
		
	}
```

### 3. 编写sql语句，执行sql语句并接收处理的结果

#### 3.1 执行sql 语句有两种方法

##### 1. executeUpdate

+ 用于 增、删、改

##### 2. executeQuery

+ 用于查询

#### 3.2 返回值有两种

##### 1. int 

+ 对应executeUpdate方法
  + 0表示sql语句执行失败
  + 大于0表示sql语句执行成功

##### 2. ResultSet 

+ 对应executeQuery 方法

``` java
package com.mrgeek.dao.impl;

import com.mrgeek.dao.StudentDao;
import com.mrgeek.entity.student;
import com.mrgeek.utils.JDBCUtil;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * 实现类
* <p>Title: StudentDaoImpl.java<／p>
* <p>Description: <／p>
* <p>Copyright: Copyright (c) 2020<／p>
* <p>Company: CUIT<／p>
* @author MrGeek
* @date 2020-09-12_17:40:20
* @version 1.0
 */
public class StudentDaoImpl implements StudentDao {

	/*
	 * 向数据库中添加数据
	 */
	public int addStudent(student student) throws Exception {
		
		int count = 0;
		
		//和数据库建立连接
		Connection connection = JDBCUtil.getConnection();
		
		//编写sql语句
		String sql = "insert into student (null,'"+student.getName()+"','"+student.getSex()+"',"+student.getYear()+")";
		
		//获取Statement 对象
		Statement statement = (Statement) connection.createStatement();
		
		//调用executeUpdate 方法，执行sql语句
		count = statement.executeUpdate(sql);
		
		//关流
		JDBCUtil.closeJDBC(statement, connection);
		
		return count;
	}

}
```

### 4. 关流

JDBC.Util.closeJDBC() 方法

```java
/**
	 * 关闭连接数据库建立的连接通道
	* <p>Title: closeConnection<／p>
	* <p>Description: <／p>
	* @param resultSet 执行sql语句返回的结果
	* @param statement Statement 对象
	* @param connection 和数据库建立的链接
	 * @throws Exception 
	 */
	public static void closeJDBC(ResultSet resultSet,Statement statement,Connection connection) throws Exception {
		
		resultSet.close();
		statement.close();
		connection.close();
	}
	
	/**
	 * 关闭数据库连接
	* <p>Title: closeJDBC<／p>
	* <p>Description: <／p>
	* @param statement
	* @param connection
	* @throws SQLException
	 */
	public static void closeJDBC(Statement statement,Connection connection) throws SQLException {
		
		statement.close();
		connection.close();
	}
```

# 完整代码：

JDBCUtil.java

``` java
package com.mrgeek.utils;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * 
* <p>Title: JDBCUtil.java<／p>
* <p>Description: JDBC工具类<／p>
* <p>Copyright: Copyright (c) 2020<／p>
* <p>Company: CUIT<／p>
* @author MrGeek
* @date 2020-09-12_16:44:11
* @version 1.0
 */
public class JDBCUtil {
	
	/**
	 * 和数据库建立连接
	* <p>Title: getConnection<／p>
	* <p>Description: <／p>
	* @return Connection 连接
	* @throws ClassNotFoundException
	* @throws SQLException
	 */
	public static Connection getConnection() throws ClassNotFoundException, SQLException {
		
		//1.加载驱动
		//确定链接的数据库的类型
		Class.forName("com.mysql.jdbc.Driver");
		//确定请求路径
		String url = "jdbc:mysql://112.124.28.54:3306/studentInfo?characterEncoding=UTF-8";
		//确定用户名
		String user = "root";
		//确定密码
		String password = "20020920z";
		
		//2.建立连接
		Connection connection = (Connection) DriverManager.getConnection(url,user,password);
		return connection;
		
	}
	
	/**
	 * 关闭连接数据库建立的连接通道
	* <p>Title: closeConnection<／p>
	* <p>Description: <／p>
	* @param resultSet 执行sql语句返回的结果
	* @param statement Statement 对象
	* @param connection 和数据库建立的链接
	 * @throws Exception 
	 */
	public static void closeJDBC(ResultSet resultSet,Statement statement,Connection connection) throws Exception {
		
		resultSet.close();
		statement.close();
		connection.close();
	}
	
	/**
	 * 关闭数据库连接
	* <p>Title: closeJDBC<／p>
	* <p>Description: <／p>
	* @param statement
	* @param connection
	* @throws SQLException
	 */
	public static void closeJDBC(Statement statement,Connection connection) throws SQLException {
		
		statement.close();
		connection.close();
	}

}

```

Dao层

```java
package com.mrgeek.dao.impl;

import com.mrgeek.dao.StudentDao;
import com.mrgeek.entity.student;
import com.mrgeek.utils.JDBCUtil;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * 实现类
* <p>Title: StudentDaoImpl.java<／p>
* <p>Description: <／p>
* <p>Copyright: Copyright (c) 2020<／p>
* <p>Company: CUIT<／p>
* @author MrGeek
* @date 2020-09-12_17:40:20
* @version 1.0
 */
public class StudentDaoImpl implements StudentDao {

	/*
	 * 向数据库中添加数据
	 */
	public int addStudent(student student) throws Exception {
		
		int count = 0;
		
		//和数据库建立连接
		Connection connection = JDBCUtil.getConnection();
		
		//编写sql语句
		String sql = "insert into student (null,'"+student.getName()+"','"+student.getSex()+"',"+student.getYear()+")";
		
		//获取Statement 对象
		Statement statement = (Statement) connection.createStatement();
		
		//调用executeUpdate 方法，执行sql语句
		count = statement.executeUpdate(sql);
		
		//关流
		JDBCUtil.closeJDBC(statement, connection);
		
		return count;
	}

}
```

