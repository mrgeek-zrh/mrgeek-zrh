---
layout:     post
title:      linux google如何安装google访问助手
subtitle:         
date:       2020-08-28
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - linux
    -   
    -   
---

# 1. 下载google访问助手

+ https://chrome.zzzmh.cn/info?token=gocklaboggjfkolaknpbhddbaopcepfp

# 2. 将下载的安装包解压

# 3. 进入解压后的文件夹，将crx 后缀的文件重命名，改为zip 后缀

# 4. 在google 拓展程序页面，打开开发者模式

# 5. 直接将改过后缀的文件拖到拓展程序页面即可自动完成安装

