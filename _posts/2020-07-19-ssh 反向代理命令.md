---
layout:     post
title:      ssh 反向代理命令
subtitle:         
date:       2020-07-19
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - ssh
    -   
    -   
---

##### 内网服务器：

> autossh -M 4516 -fCNR 4515:localhost:22 -o ServerAliveInterval=60 root@112.124.28.54 -p 22

##### 阿里云服务器：

>   ssh -p 4515 megeek@localhost