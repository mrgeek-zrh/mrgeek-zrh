---
layout:     post
title:     自己实现的C 语言库函数
subtitle:       
date:     2020-09-04
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - C
    -   
    -   
---  

### 1. strcmp 函数：

``` c
int stringCompare(char* str1,char* str2)
{
	int len1=strlen(str1),len2=strlen(str2);

	for(;*str1!='\0'&&*str2!='\0';str1++,str2++)
	{
		if (*str1-*str2!=0)
		{
			/* code */
			return *str1 - *str2;
		}
		
	}
	if (len1>len2)
	{
		/* code */
		return 1;
	}else
		return -1;

}
```

