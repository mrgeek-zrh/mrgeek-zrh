---
layout:     post
title:     springMVC 后端和前端相互传送的JSON字符串出现中文乱码
subtitle:       
date:     2020-09-15
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - Java 框架
    -   
    -   
---  

### 无论是前台向后台传送的，还是后台向前台传送的情况下，出现乱码，都可以用一下方式解决

+ 在@RequestMapping() 中加一个属性：produces = "text/plain;charset=utf-8"

> eg: 	
>
> @RequestMapping(value = "/search/{studentName}",method = RequestMethod.POST,produces = "text/plain;charset=utf-8")