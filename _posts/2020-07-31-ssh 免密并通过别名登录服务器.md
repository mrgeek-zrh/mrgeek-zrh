---
layout:     post
title:      ssh 免密登录服务器
subtitle:       ssh
date:       2020-07-31
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - ssh
    -   
    -   
---

### 1.  本机生成ssh密钥

> ssh-keygen -t rsa -C "your_email@example.com"

### 2. 将公钥（.pub 结尾的文件）上传到服务器中

+ 对于linux 、mac用户可直接使用 ：ssh-copy-id username@公网ip 将公钥上传到服务器上

### 3. 在本机的.ssh 文件夹（在 ～ 下）中新建一个config 文件，里面写入以下内容

> Host mrgeek-aliyun # 这里是你起的别名
>   HostName 112.124.28.54 # 服务器的ip
>   User root # 用户名

## 大功告成！！！

### 这样就可以直接通过 `ssh mrgeek-aliyun` 链接服务器了

