---
layout:     post
title:      Ubuntu typora 导出笔记
subtitle:         
date:       2020-09-01
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - linux
    - 

    -   
---

## 1. 下载pandoc

+ https://github.com/jgm/pandoc/releases/tag/2.10.1
+ ![image-20200901235116292](../img/noteImage/image-20200901235116292.png)

## 2. 安装

+ >  sudo dpkg -i 包名

## 3. 开启pandoc

+ > pandoc

## 然后就可以以指定的格式导出自己的笔记了

