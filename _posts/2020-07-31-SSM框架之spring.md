---
layout:     post
title:      SSM框架之spring
subtitle:       SSM框架系列
date:       2020-07-31
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - Java 框架
    -   
    -   
---

# 1. spring 的运行条件

+ spring 想要运行，至少要有一下五个包（四个核心包，一个日志包）：
  + ![image-20200731204921293](/img/noteImage/image-20200731204921293.png)

# 2. spring 的一些说明

+ spring 的体系

![image-20200802134756107](/img/noteImage/image-20200802134756107.png)

## 2.1 spring 创建对象功能

### 2.1.1 spring 什么时候创建的对象？

+ 当spring的配置文件被加载的时候，就会根据配置文件的内容创建响应的对象，并放到applicationContext 这个容器中（spring 中最大的容器，绝大部分的 对象在创建之后，都放到applicationContext 这个容器中）

### 2.1.2 spring 创建对象的三种方式

#### 2.1.2.1 通过构造方法创建

##### 1. 无参构造创建（默认情况）

+ 直接通过 `<bean id="" class=""/>` 的方式就可以创建对象

+ ```xml
  <bean id="peo" class="com.cuit.pojo"/>
  ```

##### 2. 有参构造创建：需要明确配置

+ 在使用`<bean>` 标签进行创建对象时，需要添加一些参数

+ ```xml
  <bean id="peo" class="com.cuit.pojo">
  	<constructor-arg index="0" value="123"></constructor-arg>
      <constructor-arg index="1" value="张三"></constructor-arg>
  </bean>
  ```

+ index 表示构造函数的参数的位置

  + 0 代表第一个
  + 1 代表第二个

+ value 表示对构造参数的变量赋的值

+ 对于用于 确定参数 的属性，除了index 属性，还有name等 

  + 当有多个构造方法满足条件时，默认使用`最后一个`来进行对象的创建
  + 同时，为了避免有多个构造方法满足条件，我们常同时使用多个属性，比如同时使用 index、name 属性，以确定使用某个具体的构造方法

+ ``` xml
  <bean id="peo" class="com.cuit.pojo">
  	<constructor-arg index="0" name="id" value="123"></constructor-arg>
      <constructor-arg index="1" name="mame" value="张三"></constructor-arg>
  </bean>
  ```


#### 2.1.2.2 通过实例工厂创建

+ 对于正常的实例工厂模式，想要借助工厂创建模式，必须先为工厂类创建一个对象，然后，再调用该工厂类的相应方法，创建对象

  + ```java
    PeopleFactory peopleFactory = new PeopleFactory();
    People people = peopleFactory.newInstance();
    ```

+ 上面是正常使用java代码创建对象的方法

+ 接下来是借助 `spring` 创建对象的写法

+ ```xml
  <!--为工厂类创建对象-->
  <bean id="factory" class="com.cuit.factory"/>
  <!--借助工厂类生产对象
  factory 是上面创建的工厂类对象id，newInstance 是工厂类中用于生产对象的方法名
  -->
  <bean id="peo1" factory-bean="factory" factory-method="newInstance"/>
  ```

+ 

#### 2.1.2.3 通过静态工厂创建

+ 通过一个已经存在的工厂类获取对象

+ 通过java代码的使用方法

+ ```java
  People people = PeopleFactory.newInstance();
  ```

+ 下面是借助spring进行对象创建

```xml
<!--class 是工厂类，factory-method  是工厂类中用于生产对象的方法名-->
<bean id="peo1" class="com.cuit.factory" factory-method="newInstance"/>
```

#### 2.1.2.4 实例工厂和静态工厂的区别

+ 实例工厂
  + 要先为工厂创建对象，然后才能借助工厂生产对象
+ 静态工厂
  + 可以直接调用工厂的方法，生产对象

+ 本质上：一个的方法是静态的 ，一个的方法不是静态的

## 2.2 spring 依赖注入功能（给bean的属性赋值）

### 2.2.1 借助有参构造方法给属性赋值

+ 就是前面说的

### 2.2.2 借助property属性赋值

+ 原理：借助 set 方法
  
+ 将name 的值的第一个字母大写，再加上set，然后去寻找同名的方法
  
+ ```xml
  <bean id="peo" class="com.cuit.people">
  	<property name="id" value="123"></property>
      <property name="name" value="张三"></property>
  </bean>
  ```

## 2.3 IOC、DI

### 2.3.1 IOC

+ 中文名称：控制反转。
+ 主要是为了实现与程序员的解耦合，将原来需要程序员通过new 进行实例化的过程，转交给spring 来完成

### 2.3.2 DI

+ 当一个类需要依赖另一个类（即类的某个属性是一个引用型数据类型），另一个类实例化后，注入到这个类的过程，就称为依赖注入

## 2.4 AOP

### 2.4.1 AOP中常用概念

![image-20200801151211384](/img/noteImage/image-20200801151211384.png)

### 2.4.2 AOP的实现方法

#### 2.4.2.1 通过Schema-based 实现

+ 每个通知都需要实现AfterReturningAdvice 或 BeforeReturningAdvice 接口

+ 在spring 的配置文件的`<aop:config >` 标签中配置

##### 具体实现

+ 前置通知和后置通知

```xml
<!--为前置、后置通知对应的拓展功能的实现类创建对象-->
<bean id="mybefore" class="com.cuit.mybefore"></bean>
<bean id="myafter" class="com.cuit.myafter"></bean>
<aop:config>
    <!-- com.cuit.Demo.Demo1 使用于作为切点的类的全名，括号代表这个类的参数，如果没有参数，就为空，有参数的话，就带上参数-->
	<aop:pointcut expression="execution(* com.cuit.Demo.Demo1())" id="mypoint"/>
   <!--向切点所在切面添加前置通知和后置通知-->
   <aop:advisor advice-ref="mybefore" pointcut-ref="mypoint"/> 
   <aop:advisor advice-ref="myafter" pointcut-ref="mypoint"/>
</aop:config>
```

#### 2.4.2.2 AspectJ实现

+ 每个通知不需要实现AfterReturningAdvice 或 BeforeReturningAdvice 接口

+ 配置是在spring 配置文件的`<aop:config>`的子标签 `<aop:aspect>` 中

```xml
<!--为用于处理异常的类创建对象-->
<bean id="mythrow" class="com.cuit.Throw"></bean>
<aop:config>
    <!--指定类-->
	<aop:aspcet ref="mythrow">
        <!--expression 后面是用作切点的类的全名-->
    	<aop:pointcut expression="execution(* com.cuit.test.Demo.demo1())" id="mypoint" />
        <!--method后面接的是用于处理异常的方法 point-cut 后面接的是切点
			e 表示异常的名称，加上throwing 之后，就可以在用于处理异常的方法中添加参数，并调用
		-->
        <aop:after-throwing method="myexeception" pointcut-ref="mypoint" throwing="e"/>
    </aop:aspcet>    
</aop;config>
```

​	![image-20200801170241168](/img/noteImage/image-20200801170241168.png)

## 2.5 声明式事务

### 2.5.1 声明式事务和编程式事务的区别

#### 2.5.1.1 编程式事务

+ 有程序员编程事务控制代码

#### 2.5.1.2 声明式事务

+ 事务控制代码已经由spring 写好，程序员只需声明哪些方法需要进行事务控制和如何进行事务控制

![image-20200802112908011](/img/noteImage/image-20200802112908011.png)

# 3. 测试spring是否配置成功

## 3.1 需要使用的jar包

> <!-- spring 测试所需jar 包 -->					
> 	<dependency>
> 	    <groupId>org.springframework</groupId>
> 	    <artifactId>spring-test</artifactId>
> 	    <version>4.1.6.RELEASE</version>
> 	    <scope>test</scope>
> 	</dependency>

## 3.2 编写测试代码

``` java
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring/*.xml")
public class StudentDaoTest {
	
	@Autowired
	private StudentDao studentDao;
	
	@Test
	public void testdeleteStudent() throws Exception {
		
		Student student = new Student();
		student.setId((long) 1000);
		
		int count = studentDao.deleteStudentById(student);
		System.out.println(count);
		
	}
}
```

# 4. Spring+AspectJ

Spring 与 AspectJ 集成 与直接使用AspectJ 是不同的，我们不需要定义AspectJ 类，只需要使用AspectJ切点表达式即可

## 4.1 Spring+Aspect 的使用方法

### 4.1.1 通过AspectJ execution 表达式拦截方法

**1. 先定义一个Aspect 切面类**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002231525529.png)

**@Around() 注解中参数的说明**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002231705467.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002231807652.png)

**2. 配置web.xml **

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002232113664.png)

`proxy-target-class` 属性的默认值为false ，即默认使用JDK动态代理（代理接口），当设置为true 时，则使用CGLib动态代理（代理实现类）

**3. 在代码中调用自定义Aspect类**

### 4.1.2  通过AspectJ @annotation 表达式拦截方法

**1. 自定义一个注解类**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002232438228.png)

**2. 在@annotation() 中传入自定义注解类的类名**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002232746431.png)

**3. 在想要拦截的方法上使用自定义注释**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002233149791.png)

**补充：**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002233525996.png)

### 4.1.3 引入增强

**1. 自定义一个Aspect类**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002233747591.png)

**2. 对需要引入的接口提供一个默认实现类**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002234146719.png)

**3. 使用**

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002234240466.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002234337295.png)

### 3.1.4 基于配置使用

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201002234552210.png)

# 5. 补充

## 5.1 如何中请求路径中获取变量的值

``` java
@RequestMapping(value = "/search/{studentName}",method = RequestMethod.POST,produces = "text/plain;charset=utf-8")
@ResponseBody
public String search(@PathVariable("studentName") String studentName) {
    return studentService.search(studentName);
}
```



  

