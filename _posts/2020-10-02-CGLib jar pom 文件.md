---
layout:     post
title:     CGLib jar pom 文件
subtitle:       
date:     2020-10-02
author:     MrGeek
header-img:     img/post-bg-rwd.jpg
catalog: true
tags:     
    - 其他
    -   
    -   
---  

```xml
<dependency>
    <groupId>cglib</groupId>
    <artifactId>cglib</artifactId>
    <version>2.2.2</version>
</dependency>
<dependency>
    <groupId>asm</groupId>
    <artifactId>asm</artifactId>
    <version>3.1</version>
</dependency>
```

