---
layout:     post
title:     linux minit 安装qq 微信
subtitle:       
date:     2020-09-05
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - linux
    -   
    -   
---  

### 1. 添加deepin 的软件仓库

+ ```
  wget -O- https://deepin-wine.i-m.dev/setup.sh | sh
  ```

### 2. 然后就可以安装相应的软件了

> wget http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.qq.im/deepin.com.qq.im_9.1.8deepin0_i386.deb
> wget http://mirrors.aliyun.com/deepin/pool/non-free/d/deepin.com.wechat/deepin.com.wechat_2.6.8.65deepin0_i386.deb
>
> sudo dpkg -i deepin.com.qq.im_9.1.8deepin0_i386.deb
> sudo dpkg -i deepin.com.wechat_2.6.8.65deepin0_i386.deb

### 3. 解决qq没法显示图片的问题

> sudo sysctl -w net.ipv6.conf.all.disable_ipv6=1
> sudo sysctl -w net.ipv6.conf.default.disable_ipv6=1
> sudo sysctl -w net.ipv6.conf.lo.disable_ipv6=1

