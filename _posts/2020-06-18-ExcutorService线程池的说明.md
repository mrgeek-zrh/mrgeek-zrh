---  
layout:     post
title:      ExcutorService线程池的说明
subtitle:         
date:       2020-06-18
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - Java多线程
    -   
    -   
---

# 1. ExcutorService

## 1.1 ExcutorService 是什么？

+   ExecutorService是一个线程池接口类，可以用它来创建一个线程池引用

### 1.1.1 什么是线程池？

+   简单来说，就是用来存放多个线程的池子

### 1.1.2 线程池有什么用处？

1.  重用存在的线程，减少对象创建、消亡的开销，性能佳
2.  可有效控制最大并发线程数，提高系统资源的使用率，同时避免过多资源竞争，避免堵塞。
3.  提供定时执行、定期执行、单线程、并发数控制等功能。

## 1.2 如何使用ExcutorService?

+   我们常用ExcutorService 来创建线程池。

### 1.2.1 创建线程池的具体方法：

#### 1.2.1.1 newCachedThreadPool

+   创建一个可缓存线程池。如果线程池长度超过处理需要，可灵活回收空闲线程，若无可回收，则新建线程。此线程池不会对线程池大小做限制，线程池大小完全依赖与JVM能创建的最大线程数。

#### 1.2.1.2 newFixedThreadPool

+   创建一个指定长度的线程池。可控制最大并发线程数，超出的线程会在队列中等待。

#### 1.2.1.3 newScheduleThreadPool

+   创建一个定长的线程池。支持定时及周期性任务执行。

#### 1.2.1.4 newSingleThreadExecutor

+   创建一个只有一条线程的线程池。它只会用唯一的工作线程来执行任务，保证所有任务按照指定顺序（FIFO，LIFO，优先级）执行。

#### 1.2.1.5具体代码

>   创建有三条线程的线程池
>
>   ExecotorService pool = Executors.newFixedThreadPool(3);

## 1.3 ExcutorService 配合Callable、Future类使用

+   Callable接口和Runnable接口类似，都可以用来执行任务。
    +   通过实现Callable接口执行任务，有返回值。
        +   因为call() 方法有返回值
    +   通过实现Runnable接口执行任务，没有返回值
        +   因为run() 方法没有返回值
+   Future类是ExecutorService 的submit()方法的 返回值类型
    +   submit() 方法中接收的参数可以是Callable 类型，也可以是Runnable 类型。

### 1.3.1 Callable 接口

+   Callable接口和Runnable接口类似，都可以用来执行任务。
+   实现Callable接口，必须实现call 方法

### 1.3.2 Future接口

+   用来接收submit 的返回值，也就是Callable 接口的call 方法返回结果。
    +   通过调用get() 方法获取call 的返回值。

### 1.3.3 具体代码

```java
package com.cuit.ExecutorService;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 多线程的使用展示
 * @author admin
 *
 */
public class ExecutorServiceTest {
	
	public static void main(String[] args) {
		
		//创建一个线程池，容量定义为3
		ExecutorService pool = Executors.newFixedThreadPool(3);
		
		//向线程池中添加三条线程，并用Future 类接收
		//ExecutorService的 submit() 方法既可以提交Runnable，也可以提交Callable
		Future<String>future ;
		for (int i = 0; i < 3; i++) {
			future = pool.submit(new myCallable());
			try {
				System.out.println(future.get());
			} catch (InterruptedException | ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		//关闭线程池
		pool.shutdown();
	}
}

/**
 * 通过实现Callable接口创建线程
 * 实现Callable接口要实现call 方法
 * @author admin
 *Callable 接口接收的泛型决定了call 方法的返回值类型
 */
class myCallable implements Callable<String>{

	public String call() throws Exception {
		return Thread.currentThread().getName()+"......"+"我是只定义线程，我被执行了";
	}
}

```