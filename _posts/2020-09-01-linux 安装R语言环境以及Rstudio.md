---
layout:     post
title:      linux 安装R语言环境以及Rstudio
subtitle:         
date:       2020-09-01
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - R
    -   
    -   
---

>  ①安装R（可以直接apt呦）
>
> > ```sql
> > apt-get install r-base
> > ```
>
>    ②安装Rstudio（官网下载deb包安装）
>
> > ```apache
> > wget https://download1.rstudio.org/rstudio-1.0.143-amd64.deb
> > gdebi rstudio-1.0.143-amd64.deb
> > ```