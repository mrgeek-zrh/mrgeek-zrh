---
layout:     post
title:      Ubuntu 16 搭建FastDFS
subtitle:       FastDFS
date:       2020-07-31
author:     MrGeek
header-img: img/post-bg-kuaidi.jpg
catalog: true
tags:       
    - FastDFS
    -   
    -   
---

# 1. FastDFS 简介

+ FastDFS 是一个分布式文件存储系统，，由阿里的一位大神开源。常常搭配nginx 用来存储一些静态资源,比如作为图片服务器使用。
+ 类似的系统还有HDFS、GFS等等

# 2. FastDFS 搭建

+ FastDFS 常搭配nginx 一起使用，所以在搭建FastDFS之前，我们要先搭建nginx 环境
+ 关于Nginx 搭建，见我的另一篇文章

## 2.1 下载所需安装包

## 2.2 安装

### 2.2.1 安装 libevent 工具包

1. 下载安装包

> wget https://github.com/libevent/libevent/releases/download/release-2.1.12-stable/libevent-2.1.12-stable.tar.gz

2. 解压

> tar -zxvf libevent-2.1.12-stable.tar.gz

3. 创建lib 文件夹

> mkdir /usr/lib/libevent

4. 配置(进入解压得到的文件夹内)

> ./configure -prefix=/usr/lib/libevent

5. 编译

> make

6. 安装

> make install

### 2.2.2 安装 libfastcommon库

1. 下载安装包

> wget https://github.com/happyfish100/libfastcommon/archive/V1.0.43.tar.gz

2. 解压

> tar -zxvf V1.0.43.tar.gz

3. 编译

> ./make.sh 

4. 安装

> ./make.sh install

### 2.2.3 安装Tracker

1. 下载安装文件

> wget https://github.com/happyfish100/fastdfs/archive/V6.06.tar.gz

2. 解压

> tar -zxvf V6.06.tar.gz

3. 编译

> ./make.sh

4. 安装

> ./make.sh install

安装成功后，执行如下命令，将安装目录内 conf 目录下的配置文件拷贝到 /etc/fdfs 目录下：

```
cd conf/
cp ./* /etc/fdfs/
```

5. 配置tracker

进入 /etc/fdfs/ 目录下，打开 tracker.conf 文件：

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009205927605.png)

6. 启动tracker

```
/usr/bin/fdfs_trackerd /etc/fdfs/tracker.conf start
```

可以通过查看日志，判读是否启动成功。

日志的位置：在上图中自定义的文件夹下会自动帮我们创建一个logs 文件夹，日志就在logs文件夹中

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009210240404.png)

### 2.2.4 Storage 安装

**简单起见，这里我们搭建一个 Storage 实例即可。Storage 安装也需要 libevent 和 libfastcommon，这两个库的安装参考上文，这里我不在细说。**

**Storage 本身的安装，也和 Tracker 一致，执行命令也都一样，因为我这里将 Tracker 和 Storage 安装在同一台服务器上，所以不用再执行安装命令了（相当于安装 Tracker 时已经安装了 Storage 了）。**

**唯一要做的，就是进入到 /etc/fdfs 目录下，配置 Storage：**

```
vim /etc/fdfs/storage.conf
```

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009210758801.png)

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009210944852.png)

配置完成后，执行如下命令启动 Storage：

```
/usr/bin/fdfs_storaged /etc/fdfs/storage.conf start 
```

### 2.2.5 安装 fastdfs-nginx-module

1. 下载安装包

``` 
wget https://github.com/happyfish100/fastdfs-nginx-module/archive/V1.22.tar.gz
```

2. 解压

```
tar -zxvf V1.22.tar.gz
```

3. 将 解压得到的文件夹下的 src/mod_fastdfs.conf 拷贝到 `/etc/fdfs/` 目录下，并修改该文件的内容：

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009211407789.png)

接下来，回到下载 nginx 时的解压目录中，执行如下命令，重新配置编译安装：

```text
./configure --add-module=/usr/local/fastdfs-nginx-module-1.22/src
make
make install
```

**注意路径换为自己的**

### 2.2.6 配置nginx

修改 nginx 的配置文件，如下：

```
vim /usr/local/nginx/conf/nginx.conf
```

在这里配置 nginx 请求转发

![](https://gitee.com/mrgeek-zrh/blogimage/raw/master/img/image-20201009211924339.png)

```
server {
	listen 		80;
	server_name 	自己的IP;
	location /group1/M00/{
		ngx_fastdfs_module;
	}
}
```

配置完成后，启动 nginx，看到如下日志，表示 nginx 启动成功：

````
ngx_http_fastdfs_set pid=9908
````

### 2.2.7测试是否搭建成功

进入/**etc/fdfs**,执行下面的语句

```
/usr/bin/fdfs_test /etc/fdfs/client.conf upload anti-steal.jpg
```

执行完之后，如果返回了一个网址，并且可以在浏览器中访问，说明配置成功



