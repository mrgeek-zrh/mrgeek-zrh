---
layout:     post
title:     Map集合和JSON字符串的互相转换
subtitle:       
date:     2020-09-15
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - Java基础
    -   
    -   
---  

### 1. 需要使用的jar包

> <!-- json 与字符串互转所需jar包 -->
> 	<dependency>
> 	    <groupId>commons-beanutils</groupId>
> 	    <artifactId>commons-beanutils</artifactId>
> 	    <version>1.8.0</version>
> 	</dependency>
> 	<dependency>
> 	    <groupId>commons-collections</groupId>
> 	    <artifactId>commons-collections</artifactId>
> 	    <version>3.2.1</version>
> 	</dependency>
> 	<dependency>
> 	    <groupId>commons-lang</groupId>
> 	    <artifactId>commons-lang</artifactId>
> 	    <version>2.5</version>
> 	</dependency>
> 	<dependency>
> 	    <groupId>net.sf.ezmorph</groupId>
> 	    <artifactId>ezmorph</artifactId>
> 	    <version>1.0.6</version>
> 	</dependency>
> 	<dependency>
> 	    <groupId>net.sf.json-lib</groupId>
> 	    <artifactId>json-lib</artifactId>
> 	    <version>2.4</version>
> 	</dependency>

### 2. Map 转 JSON

``` java
Map<Object, Object>map = studentService.show(page, rows);
JSONObject mapObject = JSONObject.fromObject(map);		
return mapObject.toString();
```

### 3. JSON转Map

+ 这个没有实际使用过，先记录一下参考文章
+ https://www.cnblogs.com/teach/p/5791029.html

