---
layout:     post
title:     使用git 上传本地项目到github
subtitle:       
date:     2020-09-15
author:     MrGeek
header-img:     img/post-bg-kuaidi.jpg
catalog: true
tags:     
    - git
    -   
    -   
---  

## 1. 在github 上创建一个仓库

+ 创建仓库的时候，建议让其自动创建READMR.md 文件

## 2. 使用git 将项目clone 下来

## 3. 将源代码复制粘贴到clone下来的文件夹中

## 4. 使用git 命令提交即可

